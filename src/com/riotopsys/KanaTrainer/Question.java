package com.riotopsys.KanaTrainer;

import java.io.Serializable;

public class Question implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 175959681853401087L;
	public String kana;
	public String romaji;
	public int weight;

	public Question(String kana, String romaji) {
		this.kana = kana;
		this.romaji = romaji;
		weight = 5;
	}
	
}
