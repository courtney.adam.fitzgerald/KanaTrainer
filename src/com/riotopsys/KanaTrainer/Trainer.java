package com.riotopsys.KanaTrainer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

public class Trainer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7555328267002500452L;
	private static final String TAG = Trainer.class.getSimpleName();
	private int stop;
	private int currQuestion;
	private int run;

	private Question[] list;
	private Map<String, Question> xref = new HashMap<String, Question>();

	public Trainer(Question[] list) {
		stop = 5;
		currQuestion = 0;
		run = 0;
		this.list = list;

		xref.clear();
		for (Question q : list) {
			xref.put(q.romaji, q);
		}

		shuffle(list);
		nextQuestion();
	}

	private Question[] shuffle(Question[] o) {
		Question x;
		for (int j, i = o.length; i != 0; j = (int) (Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x)
			;
		return o;
	};

	public void nextQuestion() {
		int w = qWeight();
		
		if ( w == 0 ){
			currQuestion = -1;
			return;
		}
		
		run += 1;
		int index = currQuestion;
		while (index == currQuestion) {
			int jump = (int) Math.floor(Math.random() * w / 2);
			while (jump >= 0) {
				index += 1;
				if (index >= stop) {
					index = 0;
				}
				jump -= list[index].weight;
			}
		}
		currQuestion = index;
	}

	private int qWeight() {
		int result = 0;
		for (int c = 0; c < this.stop; c++) {
			result += this.list[c].weight;
		}
		if (result <= Math.floor(stop/2 +8)) {
			stop += 2;
			if (stop >= list.length) {
				stop = list.length - 1;
			} else {
				result = qWeight();
			}
		}
		Log.i(TAG, String.format("%d, %d, %d", run, stop, result));
		return result;
	}

	public boolean check(String guess) {
		boolean result = (list[currQuestion].romaji.equals(guess));
		if (result) {
			// if (list[currQuestion].weight > 1) {
			if (list[currQuestion].weight > 0) {
				list[currQuestion].weight -= 1;
			}
//			nextQuestion();			
		} else {
			if (list[currQuestion].weight != 5) {
				list[currQuestion].weight += 1;
			}
			// raise question this one was confused with
			Question q = xref.get(guess);
			if (q != null) {
				if (q.weight != 5) {
					q.weight += 1;
				}
			}
		}
		return result;
	}
	
	public boolean finished() {
		return currQuestion == -1;
	}

	public Question currentQuestion() {
		return list[currQuestion];
	}

}