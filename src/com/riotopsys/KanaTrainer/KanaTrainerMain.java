package com.riotopsys.KanaTrainer;

import java.util.LinkedList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class KanaTrainerMain extends Activity {
	protected static final String TAG = KanaTrainerMain.class.getSimpleName();

	/** Called when the activity is first created. */

	private Trainer train;

	private EditText edittext;
	private TextView correction;
	private TextView question;
	private Button failureButton;

	private SharedPreferences perfs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		perfs = PreferenceManager.getDefaultSharedPreferences(this);
		// String fred = perfs.getString("include", "");

		edittext = (EditText) findViewById(R.id.txtAnswer);
		correction = (TextView) findViewById(R.id.lblCorrection);
		question = (TextView) findViewById(R.id.lblQuestion);
		failureButton = (Button) findViewById(R.id.Button01);

		if (savedInstanceState != null) {
			train = (Trainer) savedInstanceState.getSerializable("game");
			question.setText(train.currentQuestion().kana);

			if (savedInstanceState.getBoolean("inError")) {
				correction.setText(train.currentQuestion().romaji);
				edittext.setVisibility(View.GONE);
				failureButton.requestFocus();
				failureButton.setVisibility(View.VISIBLE);
			} else {
				correction.setText("");
				edittext.setVisibility(View.VISIBLE);
				edittext.requestFocus();
				failureButton.setVisibility(View.GONE);
			}

		} else {
			newGame();
		}

		edittext.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {

					String offered = edittext.getText().toString()
							.toLowerCase();
					if (train.check(offered)) {
						train.nextQuestion();
						if (train.finished()) {
							newGame();
							new AlertDialog.Builder(KanaTrainerMain.this)
								.setTitle("Congradulations")
								.setMessage("You've finished this game!")
								.setCancelable(true)
								.create()
								.show();
						} else {
							question.setText(train.currentQuestion().kana);
							correction.setText("");
						}
					} else {
						// Log.i(TAG, String.format("failed %s != %s",
						// train.currentQuestion().romaji, offered ));
						question.setText(train.currentQuestion().kana);
						correction.setText(train.currentQuestion().romaji);
						edittext.setVisibility(View.GONE);
						failureButton.setVisibility(View.VISIBLE);
						failureButton.requestFocus();
					}
					edittext.setText("");

					return true;
				}
				return false;
			}
		});

		failureButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				train.nextQuestion();
				question.setText(train.currentQuestion().kana);
				correction.setText("");
				edittext.setVisibility(View.VISIBLE);
				edittext.requestFocus();
				failureButton.setVisibility(View.GONE);
			}
		});

	}

	public void onSaveInstanceState(Bundle savedInstanceState) {

		savedInstanceState.putSerializable("game", train);
		savedInstanceState.putBoolean("inError",
				failureButton.getVisibility() != View.GONE);

		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menuProgress:
			break;
		case R.id.menuRestart:
			newGame();
			break;
		case R.id.menuSettings:
			Intent i = new Intent(this, Preference.class);
			startActivity(i);
			break;
		case R.id.menuQuit:
			finish();
			break;
		}
		return true;
	}

	private void newGame() {
		train = null;
		train = new Trainer(buildQuestions());

		question.setText(train.currentQuestion().kana);
		correction.setText("");
		edittext.setVisibility(View.VISIBLE);
		edittext.requestFocus();
		edittext.setText("");
		failureButton.setVisibility(View.GONE);
	}

	private Question[] buildQuestions() {

		LinkedList<Question> qs = new LinkedList<Question>();
		Question[] result = new Question[1];

		Resources res = this.getResources();
		String[] roma = res.getStringArray(R.array.romaji);
		String[] romaDia = res.getStringArray(R.array.romajiDiacritics);
		String[] kana;

		String fred = perfs.getString("include", res.getString(R.string.hira));

		if (fred.equals(res.getString(R.string.hira)) || fred.equals("Both")) {
			kana = res.getStringArray(R.array.hiragana);

			for (int c = 0; c < kana.length; c++) {
				qs.add(new Question(kana[c], roma[c]));
			}
			if (perfs.getBoolean("diacritics", false)) {
				kana = res.getStringArray(R.array.hiraganaDiacritics);
				for (int c = 0; c < kana.length; c++) {
					qs.add(new Question(kana[c], romaDia[c]));
				}
			}
		}

		if (fred.equals(res.getString(R.string.kata)) || fred.equals("Both")) {
			kana = res.getStringArray(R.array.katakana);

			for (int c = 0; c < kana.length; c++) {
				qs.add(new Question(kana[c], roma[c]));
			}
			if (perfs.getBoolean("diacritics", false)) {
				kana = res.getStringArray(R.array.katakanaDiacritics);
				for (int c = 0; c < kana.length; c++) {
					qs.add(new Question(kana[c], romaDia[c]));
				}
			}
		}

//		List<Question> bob = qs.subList(0, 10);
		
		return qs.toArray(result);
	}// close not in wp, and any not in., wo add ma nne to add cust an pn

}